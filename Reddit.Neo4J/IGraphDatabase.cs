﻿using Neo4jClient;

namespace Reddit.Neo4J
{
    public interface IGraphDatabase
    {
        IGraphClient GraphClient { get; }
    }
}
