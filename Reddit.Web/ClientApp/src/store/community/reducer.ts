import { Reducer } from 'redux';
import { AppActionTypes, LoadDataAction } from '../app/types';
import { AddPostAction, PostActionTypes } from '../post/types';
import { NormalizedObjects } from './../index';
import { CommunityActionTypes, CommunityState, JoinCommunityAction, LeaveCommunityAction } from "./types";
import { MessageActionTypes, SendMessageAction, LoadMessagesAction } from '../message/types';

const initialState: NormalizedObjects<CommunityState> = {
  allIds: [],
  byId: {},
  isLoaded: false
}

const reducer: Reducer<NormalizedObjects<CommunityState>> = (state = initialState, action) => {
  switch(action.type) {
    case AppActionTypes.FETCH_DATA: { return state; }
    case AppActionTypes.LOAD_DATA: {
      return {
        ...state,
        ...(action as LoadDataAction).appState.communities,
        isLoaded: true
      }
    }
    case CommunityActionTypes.LEAVE_COMMUNITY: {
      const {community, user} = (action as LeaveCommunityAction);
      let users: number[] = state.byId[community].users.filter(id => id !== user);
      return {
        ...state, 
        byId: {
          ...state.byId,
          [community]: {
            ...state.byId[community],
            users
          }
        }
      }
    }
    case CommunityActionTypes.JOIN_COMMUNITY: {
      const {community, user} = (action as JoinCommunityAction);
      return {
        ...state,
        byId: {
          ...state.byId,
          [community]: {
            ...state.byId[community],
            users: [...state.byId[community].users, user]
          }
        }
      }
    }
    case PostActionTypes.ADD_POST: {
      const { post } = (action as AddPostAction);
      return {
        ...state,
        byId: {
          ...state.byId,
          [post.community]: {
            ...state.byId[post.community],
            posts: [...state.byId[post.community].posts, post.id]
          }
        }
      }
    }
    case MessageActionTypes.SEND_MESSAGE: {
      const { message } = (action as SendMessageAction);
      return {
        ...state,
        byId: {
          ...state.byId,
          [message.community]: {
            ...state.byId[message.community],
            messages: [...state.byId[message.community].messages, message.id]
          }
        }
      }
    }
    default: return state;
  }
}

export { reducer as communityReducer };
