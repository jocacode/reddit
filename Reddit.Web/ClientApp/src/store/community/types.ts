import { Action } from 'redux';
import { NormalizedObjects } from '..';

export interface CommunityState {
  id: number,
  title: string,
  users: number[],
  posts: number[],
  messages: number[]
}

export enum CommunityActionTypes {
  LOAD_COMMUNITIES = "community/LOAD_COMMUNITIES",
  LEAVE_COMMUNITY = "community/LEAVE_COMMUNITY",
  JOIN_COMMUNITY = "community/JOIN_COMMUNITY",
  INIT_LEAVE_COMMUNITY = "community/INIT_LEAVE_COMMUNITY",
  INIT_JOIN_COMMUNITY = "community/INIT_JOIN_COMMUNITY"
}

export interface LoadCommunitiesAction extends Action {
  communities: NormalizedObjects<CommunityState>
}

export interface LeaveCommunityAction extends Action {
  community: number,
  user: number
}

export interface JoinCommunityAction extends Action {
  community: number,
  user: number
}

export interface InitLeaveCommunityAction extends Action {
  community: number,
  user: number
}

export interface InitJoinCommunityAction extends Action {
  community: number,
  user: number
}