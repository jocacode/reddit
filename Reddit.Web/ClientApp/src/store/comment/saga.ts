import * as saga from "redux-saga/effects";
import { API_URL } from "..";
import { apiFetch } from "../../services/auth";
import { openErrorDialog, stopSpinner } from "../ui/action";
import { DislikeCommentAction, LikeCommentAction, UserActionTypes } from "../user/types";
import { replyToComment } from "./action";
import { CommentActionTypes, ReplyToCommentAction, InitReplyToCommentAction } from "./types";

export function* commentsSaga() {
  yield saga.all([saga.fork(watchFetchRequest)]);
}

function* watchFetchRequest() {
  yield saga.takeEvery(CommentActionTypes.INIT_REPLY_TO_COMMENT, reply);
  yield saga.takeEvery(UserActionTypes.LIKE_COMMENT, likeDislikeUpdate);
  yield saga.takeEvery(UserActionTypes.DISLIKE_COMMENT, likeDislikeUpdate);
}

function* reply(action: InitReplyToCommentAction) {
  const input = {
    Content: action.comment.content,
    PostId: action.comment.postId,
    AuthorId: action.comment.authorId,
    ParentCommentId: action.comment.parentCommentId,
    CommunityTitle: action.communityTitle
  }
  const result = yield apiFetch('POST', API_URL + "Comment/ReplyToComment", input);

  if(result.success) {
    yield saga.put(replyToComment({...action.comment, id: result.data}));
  }
  else {
    yield saga.put(openErrorDialog("", "Something went wrong! Please try again!"));
  }

  yield saga.put(stopSpinner());
}

function* likeDislikeUpdate(action: LikeCommentAction | DislikeCommentAction) {
  if(action.type === UserActionTypes.LIKE_COMMENT) {
    return yield apiFetch('POST', API_URL + "Comment/LikeComment/?commentId=" 
    + action.commentId + "&userId=" + action.userId + "&communityTitle=" + action.communityTitle, "");
  }
  else {
    return yield apiFetch('POST', API_URL + "Comment/DislikeComment/?commentId=" 
    + action.commentId + "&userId=" + action.userId + "&communityTitle=" + action.communityTitle, "");
  }
}

export const getComments = (state: any) => state.comments;