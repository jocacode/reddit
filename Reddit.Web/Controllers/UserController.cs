﻿using Microsoft.AspNetCore.Mvc;
using Reddit.Business.UserManagement;
using Reddit.Business.UserManagement.Input;
using Reddit.Domain.Entities;
using Reddit.Domain.InternResults;
using Reddit.Domain.Interop;
using System.Threading.Tasks;

namespace Reddit.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [Produces("application/json")]
    public class UserController : ControllerBase
    {
        private IUserManager _userManager;

        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetUserPosts(long userId)
        {
            Result<PostsOutput> result = await _userManager.LoadUserPostsAsync(userId);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody]CreateUserInput input)
        {
            Result<long> result = await _userManager.CreateUserAsync(input);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> LogIn([FromBody]LogInInput input)
        {
            Result<User> result = await _userManager.LogIn(input);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> JoinToCommunity([FromBody]JoinLeaveInput input)
        {
            Result result = await _userManager.JoinToCommunity(input);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> LeaveCommunity([FromBody]JoinLeaveInput input)
        {
            Result result = await _userManager.LeaveCommunity(input);
            if (result.Success) return Ok(result);
            else return BadRequest(result);
        }
    }
}
