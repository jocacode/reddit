﻿namespace Reddit.Services.Repositories
{
    public enum Neo4JRelationships
    {
        HAS_POST = 1,       //(COMMUNITY -> POST)
        HAS_USER = 2,       //(COMMUNITY -> USER)
        POST_AUTHOR = 3,    //(POST -> USER) OK
        HAS_COMMENT = 4,    //(POST -> COMMENT) OK
        COMMENT_AUTHOR = 5, //(COMMENT -> USER) OK
        REPLY = 6,          //(COMMENT -> COMMENT) OK
        COMMENT_LIKE = 7,   //(USER -> COMMENT)
        COMMENT_DISLIKE = 8,//(USER -> COMMENT)
        POST_LIKE = 9,      //(USER -> POST) OK
        POST_DISLIKE = 10,  //(USER -> POST) OK
        PARENT_COMMENT = 11,  //COMMENT(reply) -> COMMENT(parent)
        MY_POST = 12,        //COMMENT -> POST
    }
}
