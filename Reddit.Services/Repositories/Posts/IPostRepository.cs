﻿using Reddit.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reddit.Services.Repositories.Posts
{
    public interface IPostRepository : INeo4JRepository<Post>
    {
        Task<List<Post>> FindManyPostsAsync(List<long> showedPostsIds, int num, string category);
        Task<List<Post>> FindMorePostsOfCommunitiesAsync(List<long> showedPostsIds, int num, string category, List<string> communityTitles);
        Task<List<Post>> GetPostsOfUserAsync(long userId);
        Task DislikePostAsync(long postId, long userId);
        Task LikePostAsync(long postId, long userId);
    }
}
