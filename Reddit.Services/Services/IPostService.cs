﻿using Reddit.Domain.Entities;

namespace Reddit.Services.Services
{
    public interface IPostService
    {
        public int ComparePosts(Post firstPost, Post secondPost);

        public double PostPopularity(Post post);
    }
}
