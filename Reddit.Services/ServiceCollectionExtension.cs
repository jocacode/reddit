﻿using Microsoft.Extensions.DependencyInjection;
using Reddit.Services.Repositories;
using Reddit.Services.Repositories.Chat;
using Reddit.Services.Repositories.Comments;
using Reddit.Services.Repositories.Communities;
using Reddit.Services.Repositories.Posts;
using Reddit.Services.Repositories.Users;
using Reddit.Services.Services;

namespace Reddit.Services
{
    public static class ServiceCollectionExtension
    {
        public static void AddRedditServices(this IServiceCollection services)
        {
            services.AddScoped<IRedisRepository, RedisRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<ICommunityRepository, CommunityRepository>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IChatRepository, ChatRepository>();
        }
    }
}
