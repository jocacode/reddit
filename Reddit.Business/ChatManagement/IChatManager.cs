﻿using Reddit.Domain.Entities;
using Reddit.Domain.Interop;
using System.Threading.Tasks;

namespace Reddit.Business.ChatManagement
{
    public interface IChatManager
    {
        Task<Result<LoadChatDataOutput>> LoadChatDataAndSubscribe(SubscribeInput input);

        Task<Result<long>> SendMessage(Message message);

        Task<Result> Subscribe(SubscribeInput input);
    }
}
