﻿namespace Reddit.Business.PostManagement.Input
{
    public class CreatePostInput
    {
        public string Content { get; set; }
        public string CommunityTitle { get; set; }
        public long AuthorId { get; set; }
    }
}
