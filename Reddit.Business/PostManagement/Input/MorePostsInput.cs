﻿using System.Collections.Generic;

namespace Reddit.Business.PostManagement.Input
{
    public class MorePostsInput
    {
        public string Category { get; set; }
        public List<long> PostsIds { get; set; }
        public long UserId { get; set; }
        public string Community { get; set; }
    }
}
