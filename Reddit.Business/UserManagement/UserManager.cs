﻿using Reddit.Business.UserManagement.Input;
using Reddit.Domain.Entities;
using Reddit.Domain.Entities.Enums;
using Reddit.Domain.InternResults;
using Reddit.Domain.Interop;
using Reddit.Services.Repositories;
using Reddit.Services.Repositories.Comments;
using Reddit.Services.Repositories.Communities;
using Reddit.Services.Repositories.Posts;
using Reddit.Services.Repositories.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reddit.Business.UserManagement
{
    public class UserManager : IUserManager
    {

        private IUserRepository _userRepository;
        private readonly IPostRepository _postRepository;
        private readonly IRedisRepository _redisRepository;
        private readonly ICommunityRepository _communityRepository;
        private readonly ICommentRepository _commentRepository;

        public UserManager(IUserRepository userRepository,
                            IPostRepository postRepository,
                            IRedisRepository redisRepository,
                            ICommunityRepository communityRepository,
                            ICommentRepository commentRepository)
        {
            _userRepository = userRepository;
            _postRepository = postRepository;
            _redisRepository = redisRepository;
            _communityRepository = communityRepository;
            _commentRepository = commentRepository;
        }

        public async Task<Result<long>> CreateUserAsync(CreateUserInput input)
        {
            User user = new User()
            {
                Email = input.Email,
                Username = input.Username,
                Password = input.Password,
                Posts = new List<long>(),
                Comments = new List<long>(),
                LikedPosts = new List<long>(),
                LikedComments = new List<long>(),
                DislikedPosts = new List<long>(),
                DislikedComments = new List<long>(),
                Communities = new List<long>()
            };

            if (!(await _userRepository.CheckUserByUsernameAsync(user.Username, user.Password)))
            {
                return new Result<long>()
                {
                    Success = true,
                    Data = await _userRepository.CreateAsync(user)
                };
            }

            return new Result<long>() { Success = false };
        }

        public async Task<Result<User>> LogIn(LogInInput input)
        {
            User user = await _userRepository.GetUserByUsernameAsync(
                input.Username, input.Password).ConfigureAwait(false);

            if (user == null) return new Result<User>() { Success = false };

            return new Result<User>() { Success = true, Data = user };
        }

        public async Task<Result<PostsOutput>> LoadUserPostsAsync(long userId)
        {
            PostsOutput posts = new PostsOutput();

            posts.Posts = await _postRepository.GetPostsOfUserAsync(userId);

            List<long> postsIds = posts.Posts.ConvertAll(new Converter<Post, long>(PostsIdsConverter));

            posts.PostsComments = await _commentRepository.FindAllCommentsOfPostsAsync(postsIds).ConfigureAwait(false);

            List<long> authorsIds = posts.PostsComments.ConvertAll(new Converter<Comment, long>(AuthorIdsConverter));
            authorsIds = authorsIds.Distinct().ToList();

            posts.Authors = await _userRepository.FindManyUsersAsync(authorsIds).ConfigureAwait(false);

            if (posts != null)
            {
                return new Result<PostsOutput>()
                {
                    Data = posts,
                    Success = true
                };
            }
            else
            {
                return new Result<PostsOutput>()
                {
                    Data = new PostsOutput(),
                    Success = false
                };
            }
        }

        public async Task<Result> JoinToCommunity(JoinLeaveInput input)
        {
            await _userRepository.JoinToCommunityAsync(
                input.CommunityId, input.UserId).ConfigureAwait(false);

            List<Community> communities = await _redisRepository
                .GetAsync<List<Community>>(RedisKeys.COMMUNITIES).ConfigureAwait(false);

            Community community = communities.Find(comm => comm.Id == input.CommunityId);

            List<User> authors = await _redisRepository
                .GetAsync<List<User>>(RedisKeys.USERS).ConfigureAwait(false);

            User user = authors.Find(user => user.Id == input.UserId);

            if (community != null)
            {
                community.Users.Add(input.UserId);
                await _redisRepository.SetAsync(
                    RedisKeys.COMMUNITIES, communities).ConfigureAwait(false);
            }

            if (user != null)
            {
                user.Communities.Add(input.CommunityId);

                await _redisRepository.SetAsync(RedisKeys.USERS, authors)
                    .ConfigureAwait(false);
            }

            return new Result() { Success = true };
        }

        public async Task<Result> LeaveCommunity(JoinLeaveInput input)
        {
            await _userRepository.LeaveCommunityAsync(
                input.CommunityId, input.UserId).ConfigureAwait(false);

            List<Community> communities = await _redisRepository
                .GetAsync<List<Community>>(RedisKeys.COMMUNITIES).ConfigureAwait(false);

            Community community = communities.Find(comm => comm.Id == input.CommunityId);

            List<User> authors = await _redisRepository
                .GetAsync<List<User>>(RedisKeys.USERS).ConfigureAwait(false);

            User user = authors.Find(user => user.Id == input.UserId);

            if (community != null)
            {
                community.Users.Remove(input.UserId);

                await _redisRepository.SetAsync(
                    RedisKeys.COMMUNITIES, communities).ConfigureAwait(false);
            }

            if (user != null)
            {
                user.Communities.Remove(input.CommunityId);

                await _redisRepository.SetAsync(
                    RedisKeys.USERS, authors).ConfigureAwait(false);
            }

            return new Result() { Success = true };
        }

        private long PostsIdsConverter(Post post)
        {
            return post != null ? post.Id : 0;
        }
        private long AuthorIdsConverter(Comment comment)
        {
            return comment != null ? comment.AuthorId : 0;
        }
        public static long UsersIdsConverter(Post post)
        {
            return post != null ? post.AuthorId : 0;
        }
    }
}
