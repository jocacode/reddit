﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reddit.Business.UserManagement.Input
{
    public class LogInInput
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
