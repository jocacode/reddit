﻿using Reddit.Business.UserManagement.Input;
using Reddit.Domain.Entities;
using Reddit.Domain.InternResults;
using Reddit.Domain.Interop;
using System.Threading.Tasks;

namespace Reddit.Business.UserManagement
{
    public interface IUserManager
    {
        Task<Result<User>> LogIn(LogInInput input);
        Task<Result<long>> CreateUserAsync(CreateUserInput input);
        Task<Result<PostsOutput>> LoadUserPostsAsync(long userId);
        Task<Result> JoinToCommunity(JoinLeaveInput input);
        Task<Result> LeaveCommunity(JoinLeaveInput input);
    }
}
