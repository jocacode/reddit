﻿using Reddit.Business.CommentManagement.Input;
using Reddit.Domain.Interop;
using System.Threading.Tasks;

namespace Reddit.Business.CommentManagement
{
    public interface ICommentManager
    {
        Task<Result<long>> ReplyToComment(CreateReplyCommentInput input);

        Task<Result> LikeComment(long commentId, long userId, string communityTitle);

        Task<Result> DislikeComment(long commentId, long userId, string communityTitle);
    }
}
