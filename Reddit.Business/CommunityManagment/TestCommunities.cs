﻿using Reddit.Domain.Entities;
using System.Collections.Generic;

namespace Reddit.Business.CommunityManagment
{
    public static class TestCommunities
    {
        public static readonly List<Community> AllCommunities = new List<Community>()
        {
            new Community()
            {
                Id = 123,
                Title = "Football",
                Users = new List<long>(),
                Posts = new List<long>(),
                Messages = new List<long>()
            },
            new Community()
            {
                Id = 1234,
                Title = "Basketball",
                Users = new List<long>(),
                Posts = new List<long>(),
                Messages = new List<long>()
            },
            new Community()
            {
                Id = 12345,
                Title = "Programming",
                Users = new List<long>(),
                Posts = new List<long>(),
                Messages = new List<long>()
            },
            new Community()
            {
                Id = 123456,
                Title = "Studying",
                Users = new List<long>(),
                Posts = new List<long>(),
                Messages = new List<long>()
            },
            new Community()
            {
                Id = 1234567,
                Title = "Fun",
                Users = new List<long>(),
                Posts = new List<long>(),
                Messages = new List<long>()
            },
            new Community()
            {
                Id = 12345678,
                Title = "Lifestyle",
                Users = new List<long>(),
                Posts = new List<long>(),
                Messages = new List<long>()
            }
        };
    }
}
